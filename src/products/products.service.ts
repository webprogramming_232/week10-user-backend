import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'orange juice', price: 80 },
  { id: 2, name: 'lemon juice', price: 80 },
  { id: 3, name: 'apple juice', price: 80 },
];
let lastProductId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    console.log({ ...createProductDto });
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto, // name, price
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log(products[index]);
    // console.log(updateProductDto);
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'orange juice', price: 80 },
      { id: 2, name: 'lemon juice', price: 80 },
      { id: 3, name: 'apple juice', price: 80 },
    ];
    lastProductId = 4;
  }
}
